var offerCount = 0;
var packages = [];
var packagesDisplay = '';
var discount = 0;
var hourlyPriceTotal = 0;
var total = 0;
var childrenCount = 0;
var familyDis = 0;
var offVal = 0.1
var familyDisSingle = 0
var totalDiscount = 0;
var values = [];
var childs = [];
let arrOfValues = [];
var totalValSingle = 0;
var hourlyPriceSingle = 0;
var discountSingle = 0
var totalValSingle = 0;
var hourlyPriceSingle = 0;
var outerhtml = [];
var fullInf = [];
var packagesCount = 0;
var mileStonesCount = 0;
var milestonesNew = 0;

function addOffer() {
    resetVal();
    values = [];
    let table = $('table');
    let ind = 0;

    if (table.length == 0) {
        var sel = $('div[offercount]');


        $.each(sel, function(index, value) {
            ind += 1;
             offerCount += 1;
            let offers = '';
            if (ind == 1) {
                offers += '<div>' +
                    '<tr>' +
                    '<td class="width-30"></td>' +
                    '<td class="weight-600">Offer ' + ind + '<a class="cursor" onclick="showMore(this)" count="' + ind + '" id="show-more" >&#8943</a></td>' +
                    '</tr>'

                    +
                    getChildrenCount() +
                    getSelectedPackage(ind)

                    +
                    '<tr>' +
                    '<td class="weight-600 width-30">Total</td>' +
                    '<td>RM' + total + '</td>' +
                    '</tr>'

                    +
                    '<tr>' +
                    '<td class="weight-600 width-30">Hourly Price</td>' +
                    '<td>' + hourlyPriceTotal + '</td>' +
                    '</tr>'

                    +
                    '<tr>' +
                    '<td class="weight-600 width-30" >Discount Girl</td>' +
                    '<td>' + '(RM' + discount + ')' + '</td>' +
                    '</tr>'

                    +
                    '<tr>' +
                    '<td class="weight-600 width-30">Fam Dis.</td>' +
                    '<td>' + '(RM' + familyDis + ')' + '</td>' +
                    '</tr>'

                    +
                    '<tr>' +
                    '<td class="weight-600 width-30" >Game</td>' +
                    '<td>' + '(' + 100 + ')' + '</td>' +
                    '</tr>'

                    +
                    '<tr>' +
                    '<td class="weight-600 width-30" >Milestone</td>' +
                    '<td>' + '(' + 0 + ')' + '</td>' +
                    '</tr>'

                    +
                    getCustomMilestones()

                    +
                    calculateDisValues()

                    +
                    calculateBankSection()

                    +
                    '</div>'


                $('#table-form').append('<table class="table table-bordered ">' +
                    '<tbody>'

                    +
                    offers +
                    '</tbody>' +
                    '</table>'


                )
            } else {
                addColumn(offerCount)
            }

        })



    } else {

        var sel2 = $('div[offercount]');

        if (sel2.length <= offerCount) {

            let ind2 = 1;
            $.each(sel2, function(index, value) {
                values = [];
                hourlyPriceTotal = 0;
                total = 0;
                ind2 += index
                var rows = $("table tr");

                let pos = 0;

                getPackages(ind2);

                values.push(packagesDisplay);
                values.push('RM' + total);
                values.push(hourlyPriceTotal);
                values.push('(RM' + discount + ')');
                values.push('(RM' + familyDis + ')');
                values.push('(' + 100 + ')');
                values.push('(' + 0 + ')');

                calculateDisValues();
                calculateBankSection();

      
                let isChanged = false;
                let numb = 9;
                    updateMilestones(true);
                ($(rows[1]).children().eq(1))[0].outerHTML = '<td class="weight-600">' + childrenCount + ' Childrens</td>'

                for (var td = 2; td < rows.length; td++) {
                    for (var item = 0; item < values.length; item++) {
                        
                        if(!isChanged && td==9){
                    
                            for(let mile = 10;mile<10+(mileStonesCount - 1);mile++){
                                 $(rows[mile])[0].remove();
                            }
                            $(rows[9])[0].outerHTML = updateMilestones();
                              isChanged = true;
                              td = 9+milestonesNew;
                              milestonesNew = 0;


                        }

                        else{
                        ($(rows[td]).children().eq(ind2))[0].outerHTML = '<td>' + values[pos] + '</td>';
                        pos = pos + 1;
                        break;
                        }
                      
                    }

                }
            })

        } else {
            addColumn(sel2.length)
            offerCount = sel2.length;   
        }

    }

}

function getChildrenCount() {
    childrenCount = $('#children-count').val()
    return '<tr><td></td><td class="weight-600" >' + childrenCount + ' Childrens</td></tr>'
}


function getSelectedPackage(ind) {
    getPackages(ind);

    return '<tr><td class="weight-600 width-30" >Package</td><td>' + packagesDisplay + '</td></tr>'
}

function addMilestone() {
    var lastMilestone = $('.milestone-custom').last();
    $(lastMilestone).children('img')[0].remove();

    $(lastMilestone).after('<div class="milestone-custom">' +
        '<input type="text" name="name">' +
        '<input type="text" name="value">' +
        '<img src="img/plus.png" id="plus-img" onclick="addMilestone()">' +
        '</div>')
}

function calculateBankSection() {
    let bankInterestValue = $('#bank-interest').val();
    let installmentMonthValue = $('#instalment').val();
    let payableAfterDiscount = total - totalDiscount;
    let ccInterest = 0;
    let PayableAfterSubcidize = 0
    let monthlyInstalment = 0

    ccInterest = payableAfterDiscount * (bankInterestValue / 100);
    PayableAfterSubcidize = payableAfterDiscount - ccInterest;
    monthlyInstalment = PayableAfterSubcidize / installmentMonthValue;

    values.push('RM' + ccInterest + ' (' + bankInterestValue + '%)');
    values.push('RM' + PayableAfterSubcidize);
    values.push('RM' + monthlyInstalment);

    return '<tr>' +
        '<td class="weight-600 width-30">CC Interest</td>' +
        '<td>RM' + ccInterest + ' (' + bankInterestValue + '%)' + '</td>' +
        '</tr>'

        +
        '<tr>' +
        '<td class="weight-600 width-30" >Payable After Subcidize</td>' +
        '<td>RM' + PayableAfterSubcidize + '</td>' +
        '</tr>'

        +
        '<tr>' +
        '<td class="weight-600 width-30" >Monthly Instalment (' + installmentMonthValue + ' Months)' + '</td>' +
        '<td>RM' + monthlyInstalment + '</td>' +
        '</tr>'
}

function calculateDisValues() {
    totalDiscount = familyDis + discount * childrenCount

    values.push('(RM' + totalDiscount + ')');
    values.push('(RM' + (offVal * (total - totalDiscount)).toFixed(3) + ')');
    values.push('RM' + (total - totalDiscount));
    values.push('RM' + (total - totalDiscount) / 1000);

    return '<tr>' +
        '<td class="weight-600 width-30">Total Discount</td>' +
        '<td>' + '(RM' + totalDiscount + ')' + '</td>' +
        '</tr>'

        +
        '<tr>' +
        '<td class="weight-600 width-30">Cronoff (10%)</td>' +
        '<td>' + '(RM' + (offVal * (total - totalDiscount)).toFixed(3) + ')' + '</td>' +
        '</tr>'

        +
        '<tr>' +
        '<td class="weight-600 width-30" >Payable After Dis.</td>' +
        '<td>' + 'RM' + (total - totalDiscount) + '</td>' +
        '</tr>'

        +
        '<tr>' +
        '<td class="weight-600 width-30" >Price After Dis.</td>' +
        '<td>RM' + (total - totalDiscount) / 1000 + '</td>' +
        '</tr>'
}

function showTotal(item) {
    var offerNumber = $(item).attr('count');
    $(item)[0].innerHTML = '&#8943';
    $(item).attr('onclick', 'showMore(this)');

    var packagesVal = [];
    var r = /\d+/g;
    var m;
    var offer = $(item).parent().parent();
    var childrens = offer.next();
    var changeitems = [];
    changeitems.push($(childrens).children().eq(offerNumber))
    var itms = $(childrens).nextAll();

    var packages = $(childrens).next().children().eq(offerNumber).text();
    var countOfChildrens = $(childrens).children().eq(offerNumber).text().match(r);

    outerhtml = [];
    let pos = 0;

    $.each(itms, function(index, value) {
        changeitems.push($(value).children().eq(offerNumber));
    })

    getOuterVal(0);

    for (var td = 0; td < changeitems.length; td++) {
        for (var item = 0; item < fullInf.length; item++) {
            $(changeitems[pos])[0].outerHTML = fullInf[pos].outerHTML;
            pos = pos + 1;
            break;
        }

    }

    fullInf = [];

}

function showMore(item) {
    var offerNumber = $(item).attr('count');
    $(item)[0].innerHTML = '&#8212';
    $(item).attr('onclick', 'showTotal(this)');
    $(item).attr('class', 'cursor');

    discount = 0;
    var packagesVal = [];
    var r = /\d+/g;
    var m;
    var offer = $(item).parent().parent();
    var childrens = offer.next();
    var changeitems = [];
    childs = [];
    changeitems.push($(childrens).children().eq(offerNumber))
    var itms = $(childrens).nextAll();

    var packages = $(childrens).next().children().eq(offerNumber).text();
    var countOfChildrens = $(childrens).children().eq(offerNumber).text().match(r);

    while ((m = r.exec(packages)) != null) {
        packagesVal.push(m[0])
    }

    $.each(packagesVal, function(index, value) {
        arrOfValues = [];
        var ind = index + 1;
        arrOfValues.push('Child ' + ind);
        arrOfValues.push(value);
        calculateHourlyPriceAndTotal(value)
        arrOfValues.push(discount);
        arrOfValues.push(0);
        getCustomMilestones();
        calculateDisValuesSingle(familyDisSingle, packagesVal.length, totalValSingle)
        calculateBankSectionSingle()


        childs.push(arrOfValues)


    });


    outerhtml = [];
    fullInf = [];

    $.each(itms, function(index, value) {
        changeitems.push($(value).children().eq(offerNumber));
    })

    getOuterVal(0);

    let pos = 0;

    for (var td = 0; td < changeitems.length; td++) {
        for (var item = 0; item < outerhtml.length; item++) {
            fullInf.push($(changeitems[pos])[0]);
            $(changeitems[pos])[0].outerHTML = outerhtml[pos];
            pos = pos + 1;
            break;
        }

    }



}

function getCustomMilestones(isNew) {
    let inputs = $('.milestone-custom  input');
    let customMilestones = '';

    for (let item = 0; item < inputs.length; item = item + 2) {
        
        if(!isNew){
            values.push($(inputs[item + 1]).val())
            mileStonesCount +=1;
        }
        else{
            milestonesNew +=1;
        }
        arrOfValues.push($(inputs[item + 1]).val())

        customMilestones += '<tr>' +
            '<td class="weight-600" >' + $(inputs[item]).val() + '</td>' +
            '<td>' + $(inputs[item + 1]).val() + '</td>' +
            '</tr>'


    }
    return customMilestones;
}

function updateMilestones(isNew){
     let inputs = $('.milestone-custom  input');
    let customMilestones = '';
  

    for (let item = 0; item < inputs.length; item = item + 2) {
          let offermilestones = ''; 
        if(!isNew){
        }
        else{
            milestonesNew +=1;
        }   

  offermilestones+=  '<td class="weight-600" >' + $(inputs[item]).val() + '</td>' +
            '<td>' + $(inputs[item + 1]).val() + '</td>'

        for(let itms = 1;itms<offerCount;itms++){
          offermilestones+= '<td>' + $(inputs[item + 1]).val() + '</td>'
        }

        customMilestones += '<tr>' +
            offermilestones +
            '</tr>'
    }
    return customMilestones;
}

function calculateHourlyPriceAndTotal(package) {
    var selectedMilestone = $("#milestonesSelect input:checked").val();

    totalValSingle = 0;
    hourlyPriceSingle = 0;
    switch (package) {
        case '100':

            if(selectedMilestone == "yes"){
                discount+=100
            }else{
                discount = 0;
            }

            totalValSingle = 53.50 * 100
            hourlyPriceSingle = 53.50

            hourlyPriceTotal += hourlyPriceSingle
            total += totalValSingle

            arrOfValues.push('RM' + totalValSingle);
            arrOfValues.push(hourlyPriceSingle);
            calculateFamilyDis(package, 53.50, 100)
            break;
        case '200':

         if(selectedMilestone == "yes"){
                discount+=200
            }else{
                discount = 0;
            }

            totalValSingle = 44.25 * 200
            hourlyPriceSingle = 44.25

            hourlyPriceTotal += hourlyPriceSingle
            total += totalValSingle

            arrOfValues.push('RM' + totalValSingle);
            arrOfValues.push(hourlyPriceSingle);
            calculateFamilyDis(package, 44.25, 200)
            break;
        case '500':

         if(selectedMilestone == "yes"){
                discount+=500
            }else{
                 discount = 0;
            }


            totalValSingle = 30.70 * 500
            hourlyPriceSingle = 30.70

            hourlyPriceTotal += hourlyPriceSingle
            total += totalValSingle

            arrOfValues.push('RM' + totalValSingle);
            arrOfValues.push(hourlyPriceSingle);
            calculateFamilyDis(package, 30.70, 500)
            break;
        case '1000':

         if(selectedMilestone == "yes"){
                discount+=1000
            }else{
                discount = 0;
            }

            totalValSingle = 26.70 * 1000
            hourlyPriceSingle = 26.70

            hourlyPriceTotal += hourlyPriceSingle
            total += totalValSingle

            arrOfValues.push('RM' + totalValSingle);
            arrOfValues.push(hourlyPriceSingle);
            calculateFamilyDis(package, 26.70, 1000)
            break;
    }
}

function calculateFamilyDis(package, hourlyPriceVal, totalVal) {
    familyDis = 0;
    familyDisSingle = 0
    arrOfValues.push("RM" + 100);

    switch (package) {
        case '100':
            if (childrenCount == 2) {
                familyDisSingle = totalVal * hourlyPriceVal * 0.02
                familyDis += familyDisSingle
            } else if (childrenCount > 2) {
                familyDisSingle = totalVal * hourlyPriceVal * 0.04
                familyDis += familyDisSingle
            }

            break;
        case '200':
            if (childrenCount == 2) {
                familyDisSingle = totalVal * hourlyPriceVal * 0.04
                familyDis += familyDisSingle
            } else if (childrenCount > 2) {
                familyDisSingle = totalVal * hourlyPriceVal * 0.06
                familyDis += familyDisSingle
            }

            break;
        case '500':
            if (childrenCount == 2) {
                familyDisSingle = totalVal * hourlyPriceVal * 0.06
                familyDis += familyDisSingle
            } else if (childrenCount > 2) {
                familyDisSingle = totalVal * hourlyPriceVal * 0.08
                familyDis += familyDisSingle
            }

            break;
        case '1000':
            if (childrenCount == 2) {
                familyDisSingle = totalVal * hourlyPriceVal * 0.08
                familyDis += familyDisSingle
            } else if (childrenCount > 2) {
                familyDisSingle = totalVal * hourlyPriceVal * 0.10
                familyDis += familyDisSingle
            }

            break;
    }
    arrOfValues.push("RM" + familyDisSingle);
}

function resetTable() {

    if($('table')[0]!=undefined){
        $('table')[0].remove()
    }

     $('.package-section')[0].remove();
    $('.girls-section')[0].remove();
    $('.bank-section')[0].remove();
        $('#calculate-btn')[0].remove();

    let inputs = $('.calculator-form input');

    for(let item=0;item<inputs.length;item++){
        $(inputs[item]).val('');
    }
   
}

function generatePackageSection() {
    let childSections = '';
    packagesCount = 1;
    for (let item = 1; item <= childrenCount; item++) {
        childSections += '<div>' +
            ' <span>Child ' + item + ' </span>' +
            '<span><input name="child' + item + ' Package' + packagesCount + '"' + ' type="radio" onclick="isGirlSection()" value="100">100 </span>' +
            '<span><input name="child' + item + ' Package' + packagesCount + '"' + ' type="radio" onclick="isGirlSection()" value="200">200 </span>' +
            '<span><input name="child' + item + ' Package' + packagesCount + '"' + ' type="radio" onclick="isGirlSection()" value="500">500 </span>' +
            '<span><input name="child' + item + ' Package' + packagesCount + '"' + ' type="radio" onclick="isGirlSection()" value="1000">1000 </span>' +
            ' </div>'
    }

    return '<div class="package-section"><h4 class="great">Package section</h4>' +
        ' </br>' +
        '<div class="offerPackages" offerCount="' + packagesCount + '">' +
        childSections +
        '</div>' +
        ' <button class="btn btn-primary" id="btn-packages" type="button" onclick="addPackages()">Add Package to compare</button>' +
        '</div>'
}

function addPackages() {
    let childSections = '';
    lastpackage = packagesCount;
    $('#btn-packages').remove();
    let lastPackage = $('div[offercount]');
    packagesCount += 1;
    for (let item = 1; item <= childrenCount; item++) {

        childSections += '<div>' +
            ' <span>Child ' + item + ' </span>' +
            '<span><input name="child' + item + ' Package' + packagesCount + '"' + ' type="radio" onclick="isGirlSection()" value="100">100 </span>' +
            '<span><input name="child' + item + ' Package' + packagesCount + '"' + ' type="radio" onclick="isGirlSection()" value="200">200 </span>' +
            '<span><input name="child' + item + ' Package' + packagesCount + '"' + ' type="radio" onclick="isGirlSection()" value="500">500 </span>' +
            '<span><input name="child' + item + ' Package' + packagesCount + '"' + ' type="radio" onclick="isGirlSection()" value="1000">1000 </span>' +
            ' </div>'


    }

    let returnhtml = '<div class="offerPackages" offerCount="' + packagesCount + '">' +
        childSections +
        '</div>'


    $('.package-section').append(returnhtml);
    $('.package-section').append(' <button class="btn btn-primary" id="btn-packages" type="button" onclick="addPackages()">Add Package to compare</button>')



}


function generateGirlsSection() {
    let girlsSections = '';

    let girlSect = $('.girls-section')[0];
    let bankSect = $('.bank-section')[0];

    if (girlSect != null) {
        $(girlSect).remove()
        $(bankSect).remove()
        $('#calculate-btn')[0].remove()
    }


    for (let item = 1; item <= childrenCount; item++) {
        girlsSections +=
            '<span>Girl ' + item + '<input name="dzen" type="checkbox" value="nedzen"><span>'
    }

    return '<div class="girls-section">' +
        '<h4 class="great">Girls section</h4>' +
        '</br>' +
        girlsSections +
        '</br>' +
        '<span>Game   <input type="text" name=""></span>' +
        '<br/>' +
        '<span id="milestonesSelect">Milestone' +
        '<span>Yes: <input name="dzen" type="radio" value="yes"></span>' +
        '<span>No: <input name="dzen" type="radio" value="no"></span>' +
        '</span>' +
        '<div class="milestone-custom">' +
        '<input type="text" name="name">' +
        '<input type="text" name="value">' +
        '<img src="img/plus.png" id="plus-img" onclick="addMilestone()">' +
        '</div>' +
        '</div>' +

        ' <div class="bank-section">' +
        '<h4 class="great">Bank section</h4>' +
        '<br/>' +
        '<div>' +
        '<span>Bank%   <input id="bank-interest" type="text" name=""></span>' +
        '<br/>' +
        '<span>Instalment   <input id="instalment" type="text" name=""></span>' +
        '</div>' +
        '</div>' +
        '<button type="button" class="btn btn-success" id="calculate-btn" onclick="addOffer()">Calclulate</button>'
}

function resetVal() {
    packages = [];
    packagesDisplay = '';
    discount = 0;
    hourlyPriceTotal = 0;
    total = 0;
    totalVal = 0;
    familyDis = 0;
    offVal = 0.1
    totalDiscount = 0;
}

function isGirlSection() {
    if ($('.package-section input:checked').length == childrenCount) {
        $('.package-section').after(generateGirlsSection());
    }

}

function addColumn(ind) {
    var rows = $("table tr");

    let pos = 0;
    values = [];
    hourlyPriceTotal = 0;
    total = 0;

    getPackages(ind);

    values.push(packagesDisplay);
    values.push('RM' + total);
    values.push(hourlyPriceTotal);
    values.push('(RM' + discount + ')');
    values.push('(RM' + familyDis + ')');
    values.push('(' + 100 + ')');
    values.push('(' + 0 + ')');
    getCustomMilestones();
    calculateDisValues();
    calculateBankSection();

    $(rows[0]).append('<td class="weight-600">Offer ' + ind + '<a class="cursor" onclick="showMore(this)" count="' + ind + '" id="show-more" >&#8943</a></td>');
    $(rows[1]).append('<td class="weight-600" >' + $('#children-count').val() + ' Childrens</td>')


    for (var td = 2; td < rows.length; td++) {
        for (var item = 0; item < values.length; item++) {
            $(rows[td]).append('<td>' + values[pos] + '</td>')
            pos = pos + 1;
            break;
        }

    }
}

function getPackages(index) {
    packagesDisplay = '';
    var selectedPackages = $(' div [offercount="' + index + '"] input:checked');

    $.each(selectedPackages, function(index, value) {
        packagesDisplay += ($(value).val()) + ','
        calculateHourlyPriceAndTotal($(value).val())
    });

    packagesDisplay = packagesDisplay.substring(0, packagesDisplay.length - 1)

}

function calculateDisValuesSingle(familyDisVal, childC, totalVal) {
    discountSingle = familyDisVal + discount * childC

    arrOfValues.push('(RM' + discountSingle + ')');
    arrOfValues.push('(RM' + (offVal * (totalVal - discountSingle)).toFixed(3) + ')');
    arrOfValues.push('RM' + (totalVal - discountSingle));
    arrOfValues.push("RM" + (totalVal - discountSingle) / 1000);

}

function calculateBankSectionSingle() {
    let bankInterestValue = $('#bank-interest').val();
    let installmentMonthValue = $('#instalment').val();
    let payableAfterDiscount = totalValSingle - discountSingle;
    let ccInterest = 0;
    let PayableAfterSubcidize = 0
    let monthlyInstalment = 0

    ccInterest = payableAfterDiscount * (bankInterestValue / 100);
    PayableAfterSubcidize = payableAfterDiscount - ccInterest;
    monthlyInstalment = PayableAfterSubcidize / installmentMonthValue;

    arrOfValues.push('RM' + ccInterest.toFixed(3) + ' (' + bankInterestValue + '%)');
    arrOfValues.push('RM' + PayableAfterSubcidize);
    arrOfValues.push('RM' + monthlyInstalment);
}

function getOuterVal(pos = 0) {
    let outerval = '';
    let count = 0;
    for (var item = 0; item < childs.length; item++) {

        outerval += '<td style="width: 150px;background-color: palegreen;text-align:center">' + childs[item][pos] + '</td>';
        count += 1;

        if (outerhtml.length == childs[item].length) {
            break;
        }

        if (count == childs.length) {
            outerhtml.push('<td align="right">' +
                '<table cellspacing="0" cellpadding="0" width="100%">' +
                '<tbody>' +
                '<tr>' +
                outerval +
                '</tr>' +
                '</tbody>' +
                '</table>' +
                '</td>');
            return getOuterVal(pos += 1)
        }

    }
}

$(document).ready(function() {
    $("#children-count").on("keyup", function() {
        let packageSect = $('.package-section')[0];

        if (packageSect != null) {
            $(packageSect).remove()
        }

        childrenCount = $(this).val();

        if (childrenCount > 0) {
            $(this).after(generatePackageSection());
        }
    });


});